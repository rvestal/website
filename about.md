---
layout: page
title: About
permalink: /about/
---

I've been in the IT/Cyber worlds for 25+ years. Currently, I am a technical engineer for a very large network company, and I wear many hats:
<ul>
<li> DataCenter Engineer/Administrator</li>
<li> Network Adminstrator</li>
<li> Storage Administrator</li>
<li> Cyber Security Analyst/PenTester</li>
</ul>
<ul>
My non-work related passions include:
<li> My family (They are awesome!)</li>
<li> Community service through <a href="https://www.gocivilairpatrol.com/" target=_blank>Civil Air Patrol</a>, Auxillary of the USAF</li>
<li> Teaching cyber security to middle and high school age students</li>
<li> Helping veterans transition into the private sector through a number of programs at my employer</li>
</ul>

I have a some repositories of scripts on <a target=_blank href="https://gitlab.com/users/rvestal/projects">gitlab.com</a> and such that I've been using over the years. Please feel free to use them and let me know of any updates you might suggest.


-Roy
