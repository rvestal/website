---
layout: windows
title: Windows SQL Cheat Sheet
---

Here some common commands I've used in the past to manage different SQL environments.

---
Find SQL Version

To show the version of SQL Server:
SELECT @@Version

---
Show Databases
To show the databases in an SQL Server:

SQL Server 2005/2008:
EXEC sp_databases
EXEC sp_helpdb

SQL Server 2000
SELECT name
FROM sys.databases
SELECT name
FROM sys.sysdatabases
Verify Database Integrity

---
Verify DB Integrity

To verify a database integrity, you can use the following syntax. This should be run on a routine basis.

DBCC CHECKDB (dbname);
GO

---
Repair Database Integrity

To repair a database integrity issue using DBCC, use the following syntax. Please Note - This can cause data loss.

ALTER DATABASE YOURDBNAME SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
BEGIN TRANSACTION;
DBCC CHECKDB ('YOURDBNAME', REPAIR_ALLOW_DATA_LOSS);
ALTER DATABASE YOURDBNAME SET MULTI_USER;
GO
Repair a SQL Server 2008 R2 DB

If an SQL Server 2008 R2 database is in "suspect" mode, this is essentially a read only mode. There are errors that the daily check has found that need to be replaced.

To repair, simply use the following script. Replace "YOUR_DB_NAME" with the db name.

PLEASE NOTE - THIS SCRIPT ALLOWS FOR DATA LOSS!! To remove this, you need to remove "REPAIR_ALLOW_DATA_LOSS". You should be prompted to repair each entry.

EXEC sp_resetstatus 'YOUR_DB_NAME';
ALTER DATABASE vcdb SET EMERGENCY
DBCC checkdb('YOUR_DB_NAME')
ALTER DATABASE YOUR_DB_NAME SET SINGLE_USER WITH ROLLBACK
IMMEDIATE
DBCC CheckDB('YOUR_DB_NAME',REPAIR_ALLOW_DATA_LOSS)
ALTER DATABASE YOUR_DB_NAME SET MULTI_USER;
GO

---
Fix DB User Logins

When restoring a DB, sometimes the users in the DB are not added back to the SQL Server. The following steps will create an SQL login for the DB user.

    Check Usernames in DB. Change DBNAME with the name of the DB in question

USE DBNAME GO EXEC sp_change_users_login 'Report' GO

    Add the username to the SQL logins. Change DBNAME with the name of the DB in question, USERNAME with the username to add, PASSWORD with the login password to use.

USE DBNAME GO EXEC sp_change_users_login 'Auto_Fix', 'USERNAME', NULL, 'PASSWORD' GO

    Map the login user to the DB user. Change DBNAME with the name of the DB in question, USERNAME with the username to add, PASSWORD with the login password to use.

USE DBNAME GO EXEC sp_change_users_login 'update_one', 'USERNAME', 'USERNAME' GO
