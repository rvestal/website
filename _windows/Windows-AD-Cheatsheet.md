---
layout: windows
title: Windows AD Cheat Sheet
---

Here are some tips and tricks that I've found during my years using and supporting Windows AD

---
Verify replication
```
C:\> repadmin /showreps
```
---
Delete Failed DC From Domain

Open Powershell "Run as Administrator" and do the following steps:

1. ntdsutil
2. metadata cleanup
3. connections
4. connect to server <server fqdn>
5. q
6. select operation target
7. list domains
8. Select domain <number>
9. List sites
10. select site <number>
11. list servers in site12. select server <number>
13. q
14. remove selected server

---
Switching the primary domain controller in Active Directory (FSMO)

There are 5 Flexible Single Master Operation (FSMO) roles:

* PDC emulator
* RID Master
* Infrastructure Master
* Schema Master
* Domain Naming Master

We have to change these settings in the following 3 locations:

*  Active Directory Domains and Trusts
* Active Directory Users and Computers
* Active Directory Schema

Steps:

1. Enable schmmgmt MMC Add-in. Open a command prompt as Administrator ("Run As Administrator") and run the following:
```
C:\> regsvr32.exe schmmgmt.dll
```
2. Launch mmc
```
C:\> Add the following snapins and click "OK":
```
3. Active Directory Domains and Trusts Active Directory Users and Computers Active Directory Schema
* Right-click "Active Directory Domains and Trusts" and select "Change Active Directory Domain Controller"
* Choose the new domain controller
* Click OK
* Right-click "Active Directory Domains and Trusts" and select "Operations Master"
* Click "Change"
* Right-click at the Users and Computers level and “Change Domain Controller”
* Right-click on the domain and choose “Operations Master"
* Open Each Tab and choose "Change"
* Click close
* Right-click “Active Directory Schema” and change the Active Directory Domain Controller.
* Right-click “Active Directory Schema” and choose “Operations Master"
* Click Change
* Click Close
