---
layout: netapp
title: Add a Shelf
---

These are the steps to use to add a new shelf. These steps are for manually adding a shelf, creating an aggregate, and adding all disks to the new aggregate

```
::> autosupport invoke -message "Adding new disk shelf" -node <cluster> -type all

::> storage disk option show 

::> storage disk option modify -node <node1>,<node2> -autoassign off

::> run -node * storage show disk -p

::> run -node * sysconfig    

::> storage disk show -container-type unassigned 

::> storage disk assign -node <node to control> -all 

::> aggr create -aggregate <new aggr> -diskcount 11 -raidtype raid_tec -node <node to control> -simulate true 

::> aggr create -aggregate <new aggre> -diskcount 11 -raidtype raid_tec -node <node to control> 

::> storage aggregate show -fields encrypt-with-aggr-key 

::> storage disk option modify -node <node1>,<node2> -autoassign on

::> storage disk option show

::> autosupport invoke -message "New shelf add complete" -node <cluster> -type all 
```