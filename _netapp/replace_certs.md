---
layout: netapp
title: Replace Certificates
---

Replacing certificates is not a difficult task, but is very detailed. Below is specifically for replacing the certificates in the root vserver that manages the ONTAP System Manager GUI

1. Show current certificates
`::> security certificate show -vserver <root vserver> -common-name <FQDN> -ca <ca>`
2. Delete specific certificate
`::> security certificate delete -common-name <hostname> -ca <CA> -type server -vserver <root vserver> -serial <SN>`
3. Create CSR request
`::> security certificate generate-csr -common-name <FQDN> -size 2048 -country <country> -state <state> -locality <locality> -organization <org> -unit <unit> -email-addr <email-address> -hash-function SHA256`
4. Install new certificate
`::> security certificate install -vserver <root vserver> -type server <- Follow all prompts`
5. Set certificate to be the certificate for ONTAP System Manager GUI
`::> security ssl modify -server-enabled true -vserver <root vserver>  -common-name <FQDN> -ca <CA>  -serial <SN>`