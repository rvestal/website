---
layout: netapp
title: NetApp Cheat Sheet
---

This is my cheat sheet of different commands that I commonly used during my years using and supporting NetApp Clusters.

----

## Basic CLI Commands for NetApp Frames 

### Show a List of Volumes
 `::> vol status`

### Take a Volume offline
 `::> vol offline <vol_name>`

 ### Destroy a Volume
 `::> vol destroy <vol_name>`

### Show a List of Aggregates
 `::> aggr status`

### Take an Aggregate offline
 `::> aggr offline <aggr_name>`

### Destroy an Aggregate
 `::> aggr destroy <aggr_name>`

### Disk Health
 ```
 ::> storage disk show
 ::> storage disk show -state broken
 ::> storage disk show -state maintenance|pending|reconstructing
 ```

### Aggregate Health
 `::> storage aggregate show -state !online`

### Volume Health
 `::> volume show -state !online`

### Vserver Health
 ```
 ::> vserver show
 ::> vserver nfs show
 ::> vserver cifs show
```

### Network Health
 ```
 ::> network interface show -status-admin down
 ::> network interface show -status-oper down
 ::> network interface show -is-home false
 ::> network port broadcast-domain show
 ::> network interface show -failover
```