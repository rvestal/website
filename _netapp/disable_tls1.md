---
layout: netapp
title: Disable TLS1.0/1.1 on NetApp ONTAP 9
---

For security reasons, TLS 1.0 and TLS 1.1 (these will inclue SSL v1, v2, v3) should be disabled. This is a simple procedure in ONTAP.</br>
</br>
**_This procedure requires a reboot of each node in the cluster_**

1. Enter advanced mode</br>
 `::> set advanced`

2. Verify current SSL configuration</br>
 `::> security config show`
3. Modify the SSL settings to ''only'' allow TLSv1.2</br>
 `::> security config modify -interface SSL -supported-protocols TLSv1.2`
4. Verify new SSL configuration
 `::> security config show`
5. Reboot each node

NetApp KB: ["Disabling TLS 1.0 and TLS 1.1 in ONTAP"](https://kb.netapp.com/Advice_and_Troubleshooting/Data_Storage_Software/ONTAP_OS/Disable_TLS_1.0_and_TLS_1.1_in_ONTAP)