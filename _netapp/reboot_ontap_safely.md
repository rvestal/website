---
layout: netapp
title: Rebooting NetApp ONTAP Safely
---

ONTAP is a *BSD based OS and as such requires a specific way to reboot each node safely. Each node "owns" at least 1 aggregate and is the backup to another node. In order to reboot each node safely, the following steps need to be followed.

2 quick definitions:
1. _takeover - 1 node taking over the servicing of an aggregate or set of aggregates from another node_
2.  _giveback - A node returning, or "giving back", an aggregate or set of aggregates to the "owner" node_

## Rebooting nodes

1. Login to consoles of each node
2. From the console of the Node1
3. Force a node to takeover aggregates from another node</br>
 `::> storage failover takeover -ofnode Node2`</br>
_This will force the node giving it's aggregates away to reboot automatically_
4. Verify the takeover is happening</br>
 `::> storage failover show-takeover`
5. Monitor the reboot of Node2 via console
6. When the login prompt returns to Node2, give the aggregates back to Node2</br>
 `::> storage failover giveback -ofnode Node2`
7. Monitor the giveback of aggregates</br>
 `::> storage failover show-giveback`
8. Verify the aggregates are on the correct Node and the node is back in HA</br>
 `::> storage failover show`

Repeat the steps for Node1 from the console of Node2 if needed