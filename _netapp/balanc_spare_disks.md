---
layout: netapp
title: Balance Spare Disks Between Nodes
---

When a failed disk is replaced with autoassign enabled, the replacement disk may be assigned to a different node than the original disk. This is called a "Spares Low" condition.

To balance the spares across the nodes, use follow the following steps to choose a spare disk(s) and change the ownership.

### Show current spare ownership
`::>storage disk show -spare`

### Disable Disk Auto Assign
`::>storage disk option modify -autoassign off -node *`

### Remove disk ownership
`::>storage disk removeowner -disk <disk number>`

### Verify disk is unassigned
`::>storage disk show -container-type unassigned`

### Assign spare disk to a specific node
`::>storage disk assign -disk <disk number> -owner <node>`