---
layout: netapp
title: Add a Admin User
---

These are the steps to use to add a new admin to a storage cluster. These steps are for manually adding a user, and creating the ssh, https, and ONTAP roles needed for an admin.

```
::*> security login create -user-or-group-name <username> -application ssh -authentication-method nsswitch -role admin

::*> security login create -user-or-group-name <username> -application ontapi -authentication-method nsswitch -role admin

::*> security login create -user-or-group-name <username> -application http -authentication-method nsswitch -role admin
```
