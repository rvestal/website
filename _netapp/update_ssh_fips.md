---
layout: netapp
title: Updating the SSH Key Exchange Algorithims for FIPS 
---

When updating a cluster to the latest ONTAP software, you may have to reset the SSH KEX in order to gain access if FIPS is enabled. To do this, use the following commands:

1. Enter advanced settings
`::> set advanced`
2. Modify ssh keys to match requested algorithms</br>
`::*> security ssh modify -vserver  -key-exchange-algorithms diffie-hellman-group-exchange-sha256,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521`