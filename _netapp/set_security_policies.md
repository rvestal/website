---
layout: netapp
title: Set Security Policies
---

Security Policies are necessary to keep unauthorized users from potentially stealing your information.

1. Set Password Policies for a specific SVM
```::>security login role config modify -vserver <SVM> -passwd-alphanum enabled -passwd-minlength 12 -passwd-min-special-chars 1 -passwd-expiry-time 60 -require-initial-passwd-update enabled -max-failed-login-attempts 3 -disallowed-reuse 24 -change-delay 1 -passwd-min-lowercase-chars 1 -passwd-min-uppercase-chars 1 -passwd-min-digits 1 -passwd-expiry-warn-time 7 -account-inactive-limit 30  *
```

2. Set Password Policies for a all SVMs
_Note this must be run before any SVMs are created. If SVMs exist, you must run these per SVM (see above)_
```
::>security login role config modify -passwd-alphanum enabled -passwd-minlength 12 -passwd-min-special-chars 1 -passwd-expiry-time 60 -require-initial-passwd-update enabled -max-failed-login-attempts 3 -disallowed-reuse 24 -change-delay 1 -passwd-min-lowercase-chars 1 -passwd-min-uppercase-chars 1 -passwd-min-digits 1 -passwd-expiry-warn-time 7 -account-inactive-limit 30  *
```
3. Set  Security Timeout
`System timeout modify 15`

4. Set Login Banner
```
::>security login banner modify 
############################## WARNING!!! #################################
################## READ THIS BEFORE ATTEMPTING TO LOGON ###################
#                                                                         #
#    This System is for the use of authorized users only.  Individuals    #
#    using this system without authority, or in excess of their           #
#    authority, are subject to having all of their activities on this     #
#    system monitored and recorded by system personnel.  In the course    #
#    of monitoring individuals improperly using this system, or in the    #
#    course of system maintenance, the activities of authorized users     #
#    may also be monitored.  Anyone using this system expressly           #
#    consents to such monitoring and is advised that if such              #
#    monitoring reveals possible criminal activity, system personnel      #
#    may provide the evidence of such monitoring to law enforcement       #
#    officials.  You cannot copy, disclose, display or otherwise          #
#    communicate the contents of this server except to other              #
#    employees who have been authorized to access this server.            #
#                                                                         #
######################### Confidential Information ########################
```