---
layout: netapp
title: Building the NetApp Simulator
---

NetApp has a simulator that can be setup as a single node or 2 node cluster. This can be used for testing and educational purposes. This page describes the setup of a dual node cluster.

_Note you will need to be registered on the NetApp support site_

### Installation

To install the simulator, you will need to download:

* The simulator OVA
* The simulator Simulate OnTAP 9.X Install and Setup Guide
* The cmode licenses file

Simulator Download Page: https://mysupport.netapp.com/site/tools/tool-eula/simulate-ontap/download

The NetApp Simulator can be installed on VMware Workstation (Windows), VMware Fusion (Mac), or VMware ESXi. Other virtualization software has not been tested. As the directions can vary, it is best to follow the Simulate OnTAP 9.X Install and Setup Guide instead of rewriting the steps from NetApp here.

#### A few installation notes:
It is recommended to setup the simulator in a 2 node cluster since this the system is the most common.</br>
Use the section of the NetApp Installation and Setup Guide called "Using Simulate ONTAP: Two Nodes". Follow the steps precisely! Any deviation and you will need to delete the VMs and start over. Pay close attention to the steps in the section "Using Simulate ONTAP: Two Nodes" of the install guide. You will need to change the system serial number of node 2 for the cluster to work correctly.

#### Once the cluster is built:
1. Disable autosupport
2. Disable logging
3. Enable Timeout (recommend 180 minutes)
4. Add NTP server, if available
5. Add addtional 14 disks. This will require a node reboot. If you are running a dual node simulator, both will need to reboot.
```
::> set diag
::>systemshell local "cd /sim/dev;sudo vsim_makedisks -t 35 -n 14 -a 2"
::>reboot local
```
6. Add 4 drives to each root aggregates (aggr0_xxxxx)
```
::> storage aggregate show-spare-disks -original-owner
::> storage aggregate add-disks -aggregate <aggr_name> -diskcount 2 -simulate true
::> storage aggregate add-disks -aggregate <aggr_name> -diskcount 2
::> storage aggregate show-status -aggregate <aggr_name>
::>>node run -node <ClusterName>
clustername> vol size vol0 +3g
clustername> vol size vol0 +200m
clustername> vol size vol0 +50m
clustername> df -hg vol0
clustername> exit
```
7. Disable all SNAPSHOT policies</br>
```
::>>node run -node <ClusterName>
clustername>snap delete -a vol0
clustername>vol size vol0 +1g
clustername>snap sched vol0 0 0 0
clustername>snap sched vol0
```
8. Install all licenses from the CModes_licenses_9.X.txt. At a minimum install cluster base, FCP, NFS, and CIFS.
9. Use the Dashboard in the GUI to auto create 2 aggregates evenly across the nodes.
10. At this point, you should be able to create SVMs, LIFs, CIFS and NFS shares.
</br>
If you would prefer a walkthrough, nazaudy.com has a good walk through using the OnTAP 9.6 Simulator. Works well with the OnTAP 9.7+ Simulators. Do the install using the Simulate OnTAP 9.X Install and Setup Guide, then start in Step 4 of the nazaudy.com walkthrough See the link below.</br>
https://www.nazaudy.com/index.php/technology/11-technology/vmware/85-netapp-simulator-9-6-p5-installation-and-configuration#Z2