---
layout: netapp
title: Add a New LUN
---

When adding a new LUN using the steps below, a new volume s created and used for each LUN. To have multiple LUNs per volume require different steps. These steps can be tested in the NetApp Simulator

### Add A Volume
These are the steps used to add a new volume to an aggregate. By default, no snapshots are enabled, and all space is provisioned at the time of creation.

`::> volume create -vserver <vserver_name> -volume <volume_name> -size {<integer>[KB|MB|GB|TB|PB]} -aggregate <aggr_name> -space-guarantee volume -snapshot-policy none -percent-snapshot-space 0 -nvfail on`

### Add A LUN
These are the steps used to add a new LUN to a volume. By default, no snapshots are enabled, and all space is provisioned at the time of creation.

`::> lun create -vserver Boot-From-SAN -path </path/to/LUN> -size {<integer>[KB|MB|GB|TB|PB]} -ostype vmware -space-reserve enabled`

### Add An iGroup
These are the steps used to add a new igroup. These igroups are used to allow WWPNs to connect to LUNs. If being used for Boot-From-SAN, each host will need a unique igroup for the BFS LUN that is only the WWPNs for that host. 

```
::> lun igroup create -vserver <vserver_name> -igroup <igroup_name> -ostype vmware -protocol fcp -initiator <WWPN>

::> lun igroup add -vserver <vserver_name> -igroup <igroup_name> -initiator <WWPN>
```
### Map An iGroup to a LUN
These is step is used to map an igroup to an existing LUN.
```
::> lun map -vserver <vserver>  </Path/To/LUN> <LUN_name> -lun-id <LUN ID>
```
