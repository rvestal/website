---
layout: linux
title: Use the Tar Command To Copy Files
---
Use the Tar Command To Copy Files

We can use tar to copy files from one directory to another, including mounted directories.

To copy the contents of one director to another:
```
$ cd /source/dir
$ tar cvf - . | ( cd /target/dir ; tar xvf - )
```

This command simply has tar use the console ( - ) as the target file for compression, immediately changes directories to the target directory, and decompresses the file (console again) .
