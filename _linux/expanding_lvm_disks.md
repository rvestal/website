---
layout: linux
title: Expanding LVM Disks in a VM
---

These are the steps used to expand a linux VM that uses LVM. It only supports ext2,3,4 file systems. xfs is not supported.

* power down VM
* add disk size
* boot
* fdisk /dev/sd??
* create partition with new space. Use disk type 8e (Linux LVM). This may require a reboot for the new partition table, YMMV.
* pvscan to get available
* pvdisplay to show space
* pvcreate /dev/sd?? to make partition available to lvm
* vgdisplay to find vol group
* vgextend volname /dev/sd??
* lvdisplay to find partition in volgroup to expand
* lvextend -L +additional-space /dev/volgroup/volname
* resize2fs /dev/volgroup/volname

Once you've completed the above, you should have a larger LVM on your VM.
