---
layout: linux
title: Set Xvnc to Export the Console
---
Here are the basic steps to export the local console to a vnc session with Xvnc. These are the basics. Other steps to limit the number of vnc sessions or the size of the sessions are not discussed here.

* Open a terminal
* As root, edit the /etc/X11/xorg.conf
* In the Section “Module”, the option “Load “vnc”” (without outside quotes) needs to be added. If the Module section is missing, add the following to the end of the /etc/X11/xorg.conf file to look like this:

  Section “Module”
  	Load “vnc”
  EndSection

In the Section “Screen”, the option “Option "SecurityTypes" "None"” (without the outside quotes) needs to be added. If this is a VM, add it under “Monitor “vmware”” line. This assumes VMware Tools is installed.
* Close the /etc/X11/xorg.conf file
* Close your terminal
* Restart X to test the Xvnc 
